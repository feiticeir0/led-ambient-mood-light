#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2561_U.h>



Adafruit_TSL2561_Unified tsl = Adafruit_TSL2561_Unified(TSL2561_ADDR_FLOAT, 12345);

/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/
void displaySensorDetails(void)
{
  sensor_t sensor;
  tsl.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" lux");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" lux");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" lux");  
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

/**************************************************************************/
/*
    Configures the gain and integration time for the TSL2561
*/
/**************************************************************************/
void configureSensor(void) {
  /* You can also manually set the gain or enable auto-gain support */
  // tsl.setGain(TSL2561_GAIN_1X);      /* No gain ... use in bright light to avoid sensor saturation */
  // tsl.setGain(TSL2561_GAIN_16X);     /* 16x gain ... use in low light to boost sensitivity */
  tsl.enableAutoRange(true);            /* Auto-gain ... switches automatically between 1x and 16x */
  
  /* Changing the integration time gives you better sensor resolution (402ms = 16-bit data) */
  tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_13MS);      /* fast but low resolution */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);  /* medium resolution and speed   */
  // tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_402MS);  /* 16-bit data but slowest conversions */

  /* Update these values depending on what you've set above! */  
  Serial.println("------------------------------------");
  Serial.print  ("Gain:         "); Serial.println("Auto");
  Serial.print  ("Timing:       "); Serial.println("13 ms");
  Serial.println("------------------------------------");
}

/* 
 Configure LED PINs
 IN LED smd, the PINs are switched
 green is R
 red is G
 blue is B
*/
int redPin = 10;   // Red LED,   connected to digital pin 9
int greenPin = 9;  // Green LED, connected to digital pin 10
int bluePin = 11;  // Blue LED,  connected to digital pin 11


// Color arrays
int black[3]         = { 0, 0, 0 };
int blue[3]          = { 0, 0, 100 };
int blue_white[3]    = { 72, 79, 100 };
int white[3]         = { 100, 100, 100 };
int yellow_white[3]  = { 100, 100, 92 };
int yellow[3]        = { 100, 100, 0 };
int red[3]           = { 100, 0, 0 };

// Set initial color
int redVal = black[0];
int greenVal = black[1]; 
int blueVal = black[2];

// Initialize color variables
int prevR = redVal;
int prevG = greenVal;
int prevB = blueVal;

int wait = 10;      // 10ms internal crossFade delay; increase for slower fades
int hold = 0;       // Optional hold when a color is complete, before the next crossFade

void setup(void) {
  Serial.begin(9600);
  Serial.println("Light Sensor Test"); Serial.println("");
  
  /* Display some basic information on this sensor */
  
  //displaySensorDetails();
  
  /* Setup the sensor gain and integration time */
  configureSensor();
  
  //configure the PINs
  pinMode(redPin, OUTPUT);   // sets the pins as output
  pinMode(greenPin, OUTPUT);   
  pinMode(bluePin, OUTPUT);
 
 //start we all off
 digitalWrite (redPin, LOW);
 digitalWrite (greenPin, LOW);
 digitalWrite (bluePin, LOW);
 
   
}

/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete (your own code
    should go here)
*/
/**************************************************************************/
void loop(void) 
{  
  /* Get a new sensor event */ 
  sensors_event_t event;
  tsl.getEvent(&event);
  
  /* Display the results (light is measured in lux) */
  if (event.light) {
    Serial.print(event.light); Serial.println(" lux");
    fadeColor (event.light);
  }
  else {
    //we dont have any info, lets fade to black
    /* If event.light = 0 lux the sensor is probably saturated
       and no reliable data could be generated! */
       crossFade(red); //red is default color
       //Serial.println("Sensor overload");
  }
  delay(250);
}


void fadeColor (int luxval) {
  
  if (luxval >= 10000.00 and luxval <= 17000.00) {
    Serial.print ("fading to blue :");
    Serial.println (luxval);
    crossFade (blue);
  } else if (luxval >= 400 and luxval < 10000.00) {
      Serial.print ("fading to blue white :");
      Serial.println (luxval);
      crossFade (blue_white);
  } else if (luxval >= 320 and luxval < 400.00) {
      Serial.print ("fading to white :");
      Serial.println (luxval);
      crossFade (white);
  } else if (luxval >= 100 and luxval < 320) {
      Serial.print ("fading to yellow white :");
      Serial.println (luxval);
      crossFade (yellow_white);
  } else if (luxval >= 80 and luxval < 100) {
      Serial.print ("fading to yellow :");
      Serial.println (luxval);
      crossFade (yellow);
  } else if (luxval < 100) {
      Serial.print ("fading to red :");
      Serial.println (luxval);
      crossFade (red);
  }
  
}

// Functions used from CrossColorFade
// http://www.arduino.cc/en/Tutorial/ColorCrossfader 

int calculateStep(int prevValue, int endValue) {
  int step = endValue - prevValue; // What's the overall gap?
  if (step) {                      // If its non-zero, 
    step = 1020/step;              //   divide by 1020
  } 
  return step;
}

/* The next function is calculateVal. When the loop value, i,
*  reaches the step size appropriate for one of the
*  colors, it increases or decreases the value of that color by 1. 
*  (R, G, and B are each calculated separately.)
*/

int calculateVal(int step, int val, int i) {

  if ((step) && i % step == 0) { // If step is non-zero and its time to change a value,
    if (step > 0) {              //   increment the value if step is positive...
      val += 1;           
    } 
    else if (step < 0) {         //   ...or decrement it if step is negative
      val -= 1;
    } 
  }
  // Defensive driving: make sure val stays in the range 0-255
  if (val > 255) {
    val = 255;
  } 
  else if (val < 0) {
    val = 0;
  }
  return val;
}

/* crossFade() converts the percentage colors to a 
*  0-255 range, then loops 1020 times, checking to see if  
*  the value needs to be updated each time, then writing
*  the color values to the correct pins.
*/

void crossFade(int color[3]) {
  // Convert to 0-255
  int R = (color[0] * 255) / 100;
  int G = (color[1] * 255) / 100;
  int B = (color[2] * 255) / 100;

  int stepR = calculateStep(prevR, R);
  int stepG = calculateStep(prevG, G); 
  int stepB = calculateStep(prevB, B);

  for (int i = 0; i <= 1020; i++) {
    redVal = calculateVal(stepR, redVal, i);
    greenVal = calculateVal(stepG, greenVal, i);
    blueVal = calculateVal(stepB, blueVal, i);

    analogWrite(redPin, redVal);   // Write current values to LED pins
    analogWrite(greenPin, greenVal);      
    analogWrite(bluePin, blueVal); 

    delay(wait); // Pause for 'wait' milliseconds before resuming the loop

  }
  // Update current values for next loop
  prevR = redVal; 
  prevG = greenVal; 
  prevB = blueVal;
  delay(hold); // Pause for optional 'wait' milliseconds before resuming the loop
}
